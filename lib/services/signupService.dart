import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:pathfinder_guild_chest/services/localStorageService.dart';
import 'package:pathfinder_guild_chest/services/loginService.dart';

String baseUrl = 'http://guild-chest-api.herokuapp.com';

ConfirmationResponse confirmationResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return ConfirmationResponse.fromJson(jsonData);
}

class ConfirmationResponse {
  String confirmedDate;

  ConfirmationResponse({
    this.confirmedDate,
  });

  factory ConfirmationResponse.fromJson(Map<String, dynamic> json) => new ConfirmationResponse(
      confirmedDate: json["confirmedDate"],
  );

  Map<String, dynamic> toJson() => {
    "confirmedDate": confirmedDate,
  };

}

Future<LoginResponse> attemptSignUp(String username, String password) async{

  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
  };

  final requestBody = json.encode({
    "email": username,
    "password": password,
  });

  final response = await http.post('$baseUrl/users/sign-up',
      headers: headers,
      body: requestBody
  );

  //todo: check response.status to see if the user was valid

  if(response.statusCode == 200) {
    final loggedInUser = loginResponseFromJson(response.body);
    storeCurrentLogin(loggedInUser);
    return loggedInUser;
  } else {
    throw 'Error attempting to sign up.  Perhaps the messenger was waylaid by orcs?';
  }

}

Future<String> attemptConfirmation(String emailAddress, String confirmationCode) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
  };

  final response = await http.get(
      '$baseUrl/auth/confirm/$emailAddress/$confirmationCode',
      headers: headers
  );

  if(response.statusCode == 200) {
    final confirmedDate = confirmationResponseFromJson(response.body).confirmedDate;
    return confirmedDate;
  } else {
    return null;
  }

}