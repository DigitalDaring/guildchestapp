import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'package:pathfinder_guild_chest/classes/userModel.dart';
import 'dart:convert';

import 'package:pathfinder_guild_chest/services/localStorageService.dart';

String baseUrl = 'http://guild-chest-api.herokuapp.com';

LoginResponse loginResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return LoginResponse.fromJson(jsonData);
}

String loginResponseToJson(LoginResponse data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class LoginResponse {
  UserModel user;
  String token;

  LoginResponse({
    this.user,
    this.token
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => new LoginResponse(
    user: UserModel.fromJson(json["user"]),
    token: json["key"],
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "key": token
  };

}

Future<LoginResponse> attemptLogin(String username, String password) async{

    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Accept': 'application/json',
    };

    final requestBody = json.encode({
      "email": username,
      "password": password,
    });

    final response = await http.post('$baseUrl/auth/login',
      headers: headers,
      body: requestBody
    );

    //todo: check response.status to see if the user was valid

    if(response.statusCode == 200) {
      final loggedInUser = loginResponseFromJson(response.body);
      storeCurrentLogin(loggedInUser);
      return loggedInUser;
    } else {
      return null;
    }

}

Future<void> attemptLogout() async {
  await wipeCurrentLogin();
}