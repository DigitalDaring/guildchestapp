import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pathfinder_guild_chest/classes/chestModel.dart';
import 'dart:async';

import 'package:pathfinder_guild_chest/classes/storedItem.dart';

String baseUrl = 'http://guild-chest-api.herokuapp.com';

Future<List<ChestModel>> listChests(String token) async{

  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final response = await http.get('$baseUrl/chests',
      headers: headers
  );

  return chestModelsArrayFromJson(response.body);
}

Future<ChestModel> createChest(String name, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "chestName": name
  });

  final response = await http.post('$baseUrl/chests/new', headers: headers, body: requestBody);

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error creating chest, this chest name may already exist.';
  }
}

Future<ChestModel> depositItemInChest(String name, String chestId, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "item": name,
    "amount": 1
  });

  final response = await http.post('$baseUrl/chests/$chestId/item/deposit', headers: headers, body: requestBody);

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error adding item to chest, this chest may be at the bottom of the sea.';
  }
  
}

Future<ChestModel> removeItemFromChest(String name, String chestId, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "item": name
  });

  final response = await http.post('$baseUrl/chests/$chestId/item/remove', headers: headers, body: requestBody);

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error removing item from chest, this chest may be a mimic!';
  }

}

Future<ChestModel> getChestDetails(String chestId, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final response = await http.get('$baseUrl/chests/$chestId/details',
      headers: headers
  );

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error finding that chest.  Perhaps it is lost in the sands of time...';
  }
}

Future<ChestModel> craftKeyForChest(String chestId, String username, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "chestId": chestId,
    "username": username,
  });

  final response = await http.post('$baseUrl/keys/craft',
      headers: headers,
      body: requestBody
  );

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error creating a key for that chest.  Perhaps it is lost in the sands of time, or maybe the user already has a key...';
  }
}

Future<ChestModel> depositGoldInChest(int amount, String chestId, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "deposit": amount,
  });

  final response = await http.post('$baseUrl/chests/$chestId/gold/deposit', headers: headers, body: requestBody);

  if(response.statusCode == 200) {
    return chestModelFromJson(response.body);
  } else {
    throw 'Error depositing gold in chest, perhaps it was cursed by a wizard?';
  }

}

// todo: build withdrawal request model maybe?
Future<bool> withdrawGoldFromChest(int amount, String chestId, String token) async {
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "withdraw": amount,
  });

  final response = await http.post('$baseUrl/chests/$chestId/gold/withdraw', headers: headers, body: requestBody);

  if(response.statusCode == 200) {
    return true;
  } else {
    throw 'Error withdrawing gold from this chest, perhaps it is an illusion?';
  }

}