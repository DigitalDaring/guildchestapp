import 'package:http/http.dart' as http;
import 'package:pathfinder_guild_chest/classes/profileModel.dart';
import 'dart:async';
import 'dart:io';
import 'package:pathfinder_guild_chest/classes/userModel.dart';
import 'dart:convert';

String baseUrl = 'http://guild-chest-api.herokuapp.com';

Future<ProfileModel> getProfile(String name, String token) async{

  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final response = await http.get('$baseUrl/profiles/$name',
      headers: headers
  );

  if(response.statusCode == 200) {
    return profileModelFromJson(response.body);
  } else {
    throw 'Error finding that user.  I fear they may have fallen in battle...';
  }
}

Future<ProfileModel> updateProfile(String name, String description, int level, int avatarId, token) async{

  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
    'Authorization': token
  };

  final requestBody = json.encode({
    "characterName": name,
    "characterDescription": description,
    "characterLevel": level,
    "avatarId": avatarId,
  });

  final response = await http.post('$baseUrl/profiles/update',
      headers: headers,
      body: requestBody
  );

  if(response.statusCode == 200) {
    return profileModelFromJson(response.body);
  } else {
    throw 'Error updating your profile.  Are you sure you\'re real?';
  }
}