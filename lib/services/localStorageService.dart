import 'dart:async';
import 'package:pathfinder_guild_chest/services/loginService.dart';
import 'package:shared_preferences/shared_preferences.dart';

void storeCurrentLogin (LoginResponse currentLogin) async {
  final prefs = await SharedPreferences.getInstance();
  final toStore = loginResponseToJson(currentLogin);
  prefs.setString("existing_user", toStore);
}

Future<void> wipeCurrentLogin() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.remove("existing_user");
}

Future<LoginResponse> getStoredLogin() async {
  final prefs = await SharedPreferences.getInstance();
  final foundResult = prefs.getString("existing_user");
  if(foundResult != null && foundResult != '') {
    return loginResponseFromJson(foundResult);
  } else {
    return null;
  }
}