// to parse, do
// final userModel = userModelFromJson(jsonString);
// got from https://app.quicktype.io

import 'dart:convert';

import 'package:pathfinder_guild_chest/classes/profileModel.dart';

UserModel userModelFromJson(String str) {
  final jsonData = json.decode(str);
  return UserModel.fromJson(jsonData);
}

String userModelToJson(UserModel data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class UserModel {
  String id;
  String email;
  String signUpDate;
  String confirmedDate;
  String confirmationCode;
  ProfileModel profile;
  String token;
  String tokenExpiration;

  UserModel({
    this.id,
    this.email,
    this.signUpDate,
    this.confirmedDate,
    this.confirmationCode,
    this.profile,
    this.token,
    this.tokenExpiration
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => new UserModel(
    id: json["_id"],
    email: json["email"],
    signUpDate: json["signUpDate"],
    confirmedDate: json["confirmedDate"],
    confirmationCode: json["confirmationCode"],
    profile: json["profile"] == null ? null : ProfileModel.fromJson(json["profile"]),
    token: json["token"],
    tokenExpiration: json["tokenExpiration"]
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "email": email,
    "signUpDate": signUpDate,
    "confirmedDate": confirmedDate,
    "profile": profile.toJson(),
    "token": token,
    "tokenExpiration": tokenExpiration
  };
}