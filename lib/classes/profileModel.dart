import 'package:pathfinder_guild_chest/classes/keyModel.dart';
import 'dart:convert';
import 'package:pathfinder_guild_chest/enums/userAvatars.dart';

ProfileModel profileModelFromJson(String str) {
  final jsonData = json.decode(str);
  return ProfileModel.fromJson(jsonData);
}

class ProfileModel {
  String characterName;
  int characterLevel;
  String characterDescription;
  List<KeyModel> keys;
  String searchCharacterName;
  int avatarId;

  ProfileModel({
    this.characterName,
    this.characterLevel,
    this.characterDescription,
    this.keys,
    this.searchCharacterName,
    this.avatarId
  });

  factory ProfileModel.fromJson(Map<String, dynamic> json) => new ProfileModel(
    characterName: json["characterName"],
    characterLevel: json["characterLevel"],
    characterDescription: json["characterDescription"],
    keys: json["keys"] == null ? [] : new List<KeyModel>.from(json["keys"].map((x) => KeyModel.fromJson(x))),
    searchCharacterName: json["searchCharacterName"],
    avatarId: json["avatarId"] == null ? DEFAULT_USER_AVATAR.id : json["avatarId"],
  );

  Map<String, dynamic> toJson() => {
    "characterName": characterName,
    "characterLevel": characterLevel,
    "characterDescription": characterDescription,
    "keys": new List<dynamic>.from(keys.map((x) => x.toJson())),
    "searchCharacterName": searchCharacterName,
    "avatarId": avatarId
  };
}