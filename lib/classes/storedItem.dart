import 'dart:convert';

StoredItem storedItemFromJson(String str) {
  final jsonData = json.decode(str);
  return StoredItem.fromJson(jsonData);
}

String storedItemToJson(StoredItem data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class StoredItem {
  double appraisedValue;
  double count;
  String name;
  String searchableName;

  StoredItem({
    this.appraisedValue,
    this.count,
    this.name,
    this.searchableName,
  });

  factory StoredItem.fromJson(Map<String, dynamic> json) => new StoredItem(
    appraisedValue: json["appraisedValue"].toDouble(),
    count: json["count"].toDouble(),
    name: json["name"],
    searchableName: json["searchableName"],
  );

  Map<String, dynamic> toJson() => {
    "appraisedValue": appraisedValue,
    "count": count,
    "name": name,
    "searchableName": searchableName,
  };
}