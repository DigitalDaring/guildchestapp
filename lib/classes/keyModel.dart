import 'dart:convert';

class KeyModel {
  String code;
  String description;
  String keyFor;
  String name;

  KeyModel({
    this.code,
    this.name,
    this.description,
    this.keyFor
  });

  factory KeyModel.fromJson(Map<String, dynamic> json) => new KeyModel(
    name: json["name"],
    code: json["code"],
    description: json["description"],
    keyFor: json["for"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "for": keyFor,
    "description": description,
    "code": code
  };

}