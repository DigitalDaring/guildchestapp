// To parse this JSON data, do
//
//     final chestLogEvent = chestLogEventFromJson(jsonString);
//     final storedItem = storedItemFromJson(jsonString);
//     final chestModel = chestModelFromJson(jsonString);

import 'dart:convert';

import 'package:pathfinder_guild_chest/classes/chestLogEvent.dart';
import 'package:pathfinder_guild_chest/classes/storedItem.dart';

ChestModel chestModelFromJson(String str) {
  final jsonData = json.decode(str);
  return ChestModel.fromJson(jsonData);
}

List<ChestModel> chestModelsArrayFromJson(String str) {
  final jsonData = json.decode(str);

  return List<ChestModel>.from(jsonData.map((x) => ChestModel.fromJson(x)));
}

String chestModelToJson(ChestModel data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ChestModel {
  String id;
  List<StoredItem> contents;
  int gold;
  List<ChestLogEvent> log;
  String name;

  ChestModel({
    this.id,
    this.contents,
    this.gold,
    this.log,
    this.name,
  });

  factory ChestModel.fromJson(Map<String, dynamic> json) => new ChestModel(
    id: json["_id"],
    contents: new List<StoredItem>.from(json["contents"].map((x) => StoredItem.fromJson(x))),
    gold: json["gold"],
    log: new List<ChestLogEvent>.from(json["log"].map((x) => ChestLogEvent.fromJson(x))),
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "contents": new List<dynamic>.from(contents.map((x) => x.toJson())),
    "gold": gold,
    "log": new List<dynamic>.from(log.map((x) => x.toJson())),
    "name": name,
  };
}



