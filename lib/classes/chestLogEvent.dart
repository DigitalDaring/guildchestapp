import 'dart:convert';

ChestLogEvent chestLogEventFromJson(String str) {
  final jsonData = json.decode(str);
  return ChestLogEvent.fromJson(jsonData);
}

String chestLogEventToJson(ChestLogEvent data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ChestLogEvent {
  String id;
  String description;
  String occurredOn;

  ChestLogEvent({
    this.id,
    this.description,
    this.occurredOn,
  });

  factory ChestLogEvent.fromJson(Map<String, dynamic> json) => new ChestLogEvent(
    id: json["_id"],
    description: json["description"],
    occurredOn: json["occurredOn"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "description": description,
    "occurredOn": occurredOn,
  };
}