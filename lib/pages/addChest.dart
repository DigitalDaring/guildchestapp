import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class AddChestPage extends StatefulWidget {
  AddChestPage({Key key}) : super(key: key);

  @override
  _AddChestPageState createState() => _AddChestPageState();
}

class _AddChestPageState extends State<AddChestPage> {

  final _nameController = TextEditingController();

  void _createChest() async{
    final chestName = _nameController.value.text;
    final token = userState.token;

    var createdChest = await createChest(chestName, token);
    allChestsState.add(createdChest);
    Navigator.of(context).pushNamedAndRemoveUntil('welcome', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create New Chest')
      ),
      body: Center(
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                labelText: 'Chest Name'
              ),
              controller: _nameController,
            ),
            FlatButton(
              child: Text('Create Chest'),
              onPressed: _createChest,
            )
          ],
        )
      )
    );
  }
}