import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/classes/userModel.dart';
import 'package:pathfinder_guild_chest/components/ItemComponent.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';
import 'package:pathfinder_guild_chest/state/pagesState.dart';

class ChestDetailsPage extends StatefulWidget {

  @override
  _ChestDetailsPage createState() => _ChestDetailsPage();

}

class _ChestDetailsPage extends State<ChestDetailsPage> {

  UserModel _currentUser;
  var _listItems = [];

  @override
  initState(){
    super.initState();

    setState(() {

      _currentUser = userState.user;
      chestState.contents.sort((a, b) =>
          a.searchableName.compareTo(b.searchableName)
      );
      _listItems = chestState.contents;

    });

  }

  Future<void> _reloadChestDetails() async {
    var chestDetails = await getChestDetails(chestState.id, userState.token);
    chestState = chestDetails;
    setState(() {
      chestState.contents.sort((a, b) =>
          a.searchableName.compareTo(b.searchableName)
      );
      _listItems = chestState.contents;
    });
  }

  void _goToDepositItemPage() {
    Navigator.of(context).pushNamed("addItemToChest")
      .then((value) async {
        _reloadChestDetails();
      });
  }

  void _goToCraftKeyPage() {
    Navigator.of(context).pushNamed('craftAKey')
      .then((value) async {
        _reloadChestDetails();
     });
  }

  void _goToDepositGoldPage() {
    Navigator.of(context).pushNamed('manageGold')
        .then((value) async {
      _reloadChestDetails();
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(chestState.name),
      ),
        body: Center(
          child: RefreshIndicator(
            onRefresh: _reloadChestDetails,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Text(
                            'Contains ' + chestState.gold.toString() + ' gold'
                        ),
                        Row(
                          children: [
                            FlatButton(
                              child: Text('Manage Gold'),
                              onPressed: _goToDepositGoldPage,
                            ),
                          ]
                        )
                      ]
                    )
                ),
                Flexible(
                    child: ListView.builder(
                      itemCount: _listItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        final currentItem = _listItems[index];
                        return GestureDetector(
                            onTap: () {
                            },
                            child:
                              Dismissible (
                                background: Container(color: Colors.red),
                                key: Key(currentItem.searchableName),
                                onDismissed: (direction) async {
                                  setState(() {
                                    _listItems.removeAt(index);
                                  });
                                  await removeItemFromChest(
                                      currentItem.searchableName,
                                      chestState.id,
                                      userState.token);
                                  _reloadChestDetails();
                                },
                                child: ItemComponent(
                                  item: currentItem,
                                  onPressed: () {},
                                )
                              )
                        );
                      },
                    )
                ),
                FlatButton(
                  child: Text('Share a Key'),
                  onPressed: _goToCraftKeyPage,
                ),
                FlatButton(
                  child: Text('Deposit Item'),
                  onPressed: _goToDepositItemPage,
                ),
              ],
            ),
          ),
        )
    );
  }
}