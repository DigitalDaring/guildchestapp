import 'package:flutter/material.dart';
import 'dart:async';

import 'package:pathfinder_guild_chest/services/loginService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class LogoutPage extends StatefulWidget {
  LogoutPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LogoutPageState createState() => _LogoutPageState();
}

class _LogoutPageState extends State<LogoutPage> {

  var _loggedOut = false;

  void _attemptLogout() async {
    await attemptLogout();
    userState = null;
    setState((){
      _loggedOut = true;
    });
  }
  
  @override
  void initState() {
    super.initState();
    _attemptLogout();
  }
  
  void _returnToLoginScreen(){
    Navigator.of(context).pushReplacementNamed('login');
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text("Login to Guild Chest"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(_loggedOut ? 'Successfully Logged Out!' : 'Logging You Out . . .'),
              FlatButton(
                child: Text('Return To Login'),
                onPressed: _returnToLoginScreen,
              ),
            ],
          ),
        )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}
