import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';
import 'package:pathfinder_guild_chest/services/profileService.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class ManageGoldPage extends StatefulWidget {
  ManageGoldPage({Key key}) : super(key: key);

  @override
  _ManageGoldPageState createState() => _ManageGoldPageState();
}

class _ManageGoldPageState extends State<ManageGoldPage> {

  final _nameController = TextEditingController();
  var _errorMessage = '';

  void _changeGold(bool isWithdrawal, int amount) async {
    final token = userState.token;

    try {
      if(isWithdrawal) {
        await withdrawGoldFromChest(amount, chestState.id, token);
      } else {
        chestState = await depositGoldInChest(amount, chestState.id, token);
      }

      Navigator.of(context).pop();
    } catch (error) {
      this.setState(() {
        _errorMessage = error;
      });
    }

  }

  void _withdrawGold() async {
    bool isInt = true;
    final withdrawAmount = _nameController.value.text;

    try {
      int.parse(withdrawAmount);
    } catch (error) {
      _errorMessage = 'Please enter a whole number';
    }

    if (isInt) {
      _changeGold(true, int.parse(withdrawAmount));
    }
  }

  void _depositGold() async {
    bool isInt = true;
    final depositAmount = _nameController.value.text;

    try {
      int.parse(depositAmount);
    } catch (error) {
      _errorMessage = 'Please enter a whole number';
    }

    if (isInt) {
      _changeGold(false, int.parse(depositAmount));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Managing Gold for ' + chestState.name)
        ),
        body: Center(
            child: Column(
              children: [
                Text('Currently at: ' + chestState.gold.toString() + ' gold'),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Amount'
                  ),
                  controller: _nameController,
                ),
                Text(_errorMessage),
                FlatButton(
                  child: Text('Deposit'),
                  onPressed: _depositGold,
                ),
                FlatButton(
                  child: Text('Withdraw'),
                  onPressed: _withdrawGold,
                )
              ],
            )
        )
    );
  }

}