import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class AddItemToChestPage extends StatefulWidget {
  AddItemToChestPage({Key key}) : super(key: key);

  @override
  _AddItemToChestPageState createState() => _AddItemToChestPageState();
}

class _AddItemToChestPageState extends State<AddItemToChestPage> {

  final _nameController = TextEditingController();

  void _addItemToChest() async{
    final itemName = _nameController.value.text;
    final token = userState.token;

    chestState = await depositItemInChest(itemName, chestState.id, token);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Add to ' + chestState.name)
        ),
        body: Center(
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Item Name'
                  ),
                  controller: _nameController,
                ),
                FlatButton(
                  child: Text('Add Item to Chest'),
                  onPressed: _addItemToChest,
                )
              ],
            )
        )
    );
  }

}