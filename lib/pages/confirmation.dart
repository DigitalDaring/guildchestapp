import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/services/localStorageService.dart';
import 'dart:async';
import 'package:pathfinder_guild_chest/services/signupService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class ConfirmPage extends StatefulWidget {
  ConfirmPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ConfirmPageState createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {

  final _codeController = TextEditingController();
  var _confirmationFailed = false;

  void _attemptConfirmation() async {
    try {
        this.setState(() {
          _confirmationFailed = false;
        });
        final confirmationResult = await attemptConfirmation(
            userState.user.email, _codeController.value.text
        );

        if(confirmationResult != null) {
          userState.user.confirmedDate = confirmationResult;
          storeCurrentLogin(userState);
          Navigator.of(context).pushReplacementNamed("welcome");
        } else {
          throw 'Confirmation Failed';
        }

    } catch(e) {
      this.setState(() {
        _confirmationFailed = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Text(
                      'Confirm Signup',
                      textScaleFactor: 2.2,
                    ),
                    Image(image: AssetImage("assets/gui/rc_t_02.png")),
                    TextField(
                      decoration: InputDecoration(
                          labelText: 'Confirmation Code'
                      ),
                      controller: _codeController,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 6.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            RaisedButton(
                              color: Theme.of(context).accentColor,
                              child: Text('Confirm', textScaleFactor: 1.4),
                              onPressed: _attemptConfirmation,
                            ),
                          ]
                      ),
                    ),
                    Text(_confirmationFailed ?
                    "Sorry, something went wrong with confirmation!" : ""),
                  ]
                ),
              )
            ],
          ),
        )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void dispose() {
    _codeController.dispose();
    super.dispose();
  }
}
