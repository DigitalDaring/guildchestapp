import 'package:flutter/material.dart';
import 'dart:async';
import 'package:pathfinder_guild_chest/services/signupService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class SignupPage extends StatefulWidget {
  SignupPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordConfirmController = TextEditingController();
  var _signupError = '';

  void _attemptSignUp() async {
    try {
      if(_passwordController.value.text.length > 5 &&
        _passwordController.value.text == _passwordConfirmController.value.text &&
        _emailController.value.text != ''
      ) {
        final signupResult = await attemptSignUp(
            _emailController.value.text, _passwordController.value.text);
        userState = signupResult;
        Navigator.of(context).pushReplacementNamed("confirm");
      } else {
        this.setState(() {
          _signupError = 'Please enter an email address and a password longer than 5 characters.';
        });
      }
    } catch(e) {
      userState = null;
      this.setState(() {
        _signupError = e;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Text(
                      'Signup for Guild Chest',
                      textScaleFactor: 2.2,
                    ),
                    Image(image: AssetImage("assets/gui/book_t_01.png")),
                    TextField(
                      decoration: InputDecoration(
                          labelText: 'Email Address'
                      ),
                      controller: _emailController,
                    ),
                    TextField(
                      decoration: InputDecoration(
                          labelText: 'Password'
                      ),
                      controller: _passwordController,
                      obscureText: true,
                    ),
                    TextField(
                      decoration: InputDecoration(
                          labelText: 'Confirm Password'
                      ),
                      controller: _passwordConfirmController,
                      obscureText: true,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 6.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            RaisedButton(
                              color: Theme.of(context).accentColor,
                              child: Text('Signup', textScaleFactor: 1.4),
                              onPressed: _attemptSignUp,
                            ),
                          ]
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(14.0),
                      child:  Text(_signupError.length > 0 ?
                      _signupError : ""),
                    )
                  ]
                )
              )
            ],
          ),
        )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _passwordConfirmController.dispose();
    super.dispose();
  }
}
