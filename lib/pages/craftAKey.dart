import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';
import 'package:pathfinder_guild_chest/services/profileService.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class CraftAKeyPage extends StatefulWidget {
  CraftAKeyPage({Key key}) : super(key: key);

  @override
  _CraftAKeyPageState createState() => _CraftAKeyPageState();
}

class _CraftAKeyPageState extends State<CraftAKeyPage> {

  final _nameController = TextEditingController();
  var _errorMessage = '';

  void _craftKeyForChest() async{
    final username = _nameController.value.text;
    final token = userState.token;

    try {
      chestState = await craftKeyForChest(chestState.id, username, token);
      Navigator.of(context).pop();
    } catch (error) {
      this.setState(() {
        _errorMessage = error;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Send a key for ' + chestState.name)
        ),
        body: Center(
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                      labelText: 'User Name'
                  ),
                  controller: _nameController,
                ),
                Text(_errorMessage),
                FlatButton(
                  child: Text('Craft Key'),
                  onPressed: _craftKeyForChest,
                )
              ],
            )
        )
    );
  }

}