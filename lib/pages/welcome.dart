import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/classes/chestModel.dart';
import 'dart:async';

import 'package:pathfinder_guild_chest/classes/userModel.dart';
import 'package:pathfinder_guild_chest/components/ChestComponent.dart';
import 'package:pathfinder_guild_chest/enums/userAvatars.dart';
import 'package:pathfinder_guild_chest/services/localStorageService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';
import 'package:pathfinder_guild_chest/state/chestState.dart';
import 'package:pathfinder_guild_chest/services/chestService.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

  UserModel _currentUser;
  String _welcomeText = '';

  var _listItems = [];

  @override
  initState(){
    super.initState();

    setState(() {

      _currentUser = userState.user;
      if(_currentUser.profile != null && _currentUser.profile.characterName != null) {
        _welcomeText = 'Welcome, ' + _currentUser.profile.characterName;
      } else {
        _welcomeText = 'Welcome to Guild Chest!';
      }

    });

    _loadChests();

  }

  Future<void> _loadChests() async {
    allChestsState = await listChests(userState.token);
    setState(() {
      _listItems = allChestsState;
    });
  }

  void _goToProfilePage() {
    Navigator.of(context).pushNamed("profile");
  }

  void _goToLogoutPage() {
    Navigator.of(context).pushReplacementNamed("logout");
  }

  void _goToAddChestPage() {
    Navigator.of(context).pushNamed("addchest");
  }

  void _goToChestDetailsPage(ChestModel chest) {
    chestState = chest;
    Navigator.of(context).pushNamed("chestdetails");
  }

  Widget _makeBottom() {

    var currentProfile = userState.user.profile;

    var profileImageUri = currentProfile == null
        ? DEFAULT_USER_AVATAR.uri
        : USER_AVATARS.firstWhere(
            (avatar) => avatar.id == currentProfile.avatarId
        ).uri;

    return Container(
        height: 140.0,
        child: BottomAppBar(
          color: Theme.of(context).primaryColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  FlatButton(
                    child: Stack(
                      children: [
                        Image(
                          width: 64.0,
                          height: 64.0,
                          fit: BoxFit.contain,
                          image: AssetImage(profileImageUri)
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: Icon (
                            Icons.edit,
                            color: Colors.white,
                          )
                        )
                      ],
                    ),
                    onPressed: _goToProfilePage,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        currentProfile == null ?
                        'No Profile' : currentProfile.characterName,
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.left,),
                      Text(
                        currentProfile== null ?
                        'Create One' : 'Chest Keys: '
                            + currentProfile.keys.length.toString(),
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.left),
                    ]
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 4.0, 0.0),
                child: Column(
                    children: [
                      RaisedButton(
                        color: Theme.of(context).accentColor,
                        child: Text('Add a Chest'),
                        onPressed: _goToAddChestPage,
                      ),
                      FlatButton(
                        child: Text(
                          'Logout',
                          style: TextStyle(color: Colors.white70),
                        ),
                        onPressed: _goToLogoutPage,
                      )
                    ]
                )
              )
            ]
          )
        )
    );

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(_welcomeText),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                    'Chests that you have keys to:'
                )
              ),
              Flexible(
                child: RefreshIndicator(
                  onRefresh: _loadChests,
                  child: ListView.builder(
                    itemCount: _listItems.length,
                    itemBuilder: (BuildContext context, int index) {
                      final currentChest = _listItems[index];
                      return ChestComponent(chest: currentChest, onPressed: () {
                        _goToChestDetailsPage(currentChest);
                      });
                    },
                  )
                )
              )
            ],
          ),
        ),
        bottomNavigationBar: _makeBottom()
    );
  }
}