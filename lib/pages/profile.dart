import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/classes/profileModel.dart';
import 'dart:async';

import 'package:pathfinder_guild_chest/classes/userModel.dart';
import 'package:pathfinder_guild_chest/enums/userAvatars.dart';
import 'package:pathfinder_guild_chest/services/profileService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  UserModel _currentUser;
  String _currentToken;
  ProfileModel _loadedProfile;
  String _titleText = '';
  int _selectedAvatarId = DEFAULT_USER_AVATAR.id;

  final _nameController = TextEditingController();
  final _levelController = TextEditingController();
  final _descriptionController = TextEditingController();

  void _performProfileLoad() async {
    try {
      _loadedProfile = await getProfile(
          _currentUser.profile.characterName,
          _currentToken
      );

      _selectedAvatarId = _loadedProfile.avatarId == null
        ? DEFAULT_USER_AVATAR.id
        : _loadedProfile.avatarId;

      userState.user.profile = _loadedProfile;
      _setTextValues(_loadedProfile);

    } catch(e) {
      _titleText = 'something went wrong!';
    }
  }

  void _setTextValues(ProfileModel profile) {
    setState(() {

      if(profile.characterName != '') {
        _titleText = 'Edit Your Profile';
      } else {
        _titleText = 'Create Your Profile';
      }

      _nameController.text = profile.characterName;
      _levelController.text = (
          profile.characterLevel == null
          || profile.characterLevel == ''
          ) ? 1 : profile.characterLevel.toString();
      _descriptionController.text = profile.characterDescription;
    });
  }

  @override
  initState(){
    super.initState();

      _currentUser = userState.user;
      _currentToken = userState.token;

      if(_currentUser.profile != null) {
        _titleText = 'Edit Your Profile';
        _performProfileLoad();
      } else {

        _loadedProfile = new ProfileModel(
            characterName: '',
            characterDescription: '',
            characterLevel: 1,
            searchCharacterName: ''
        );

        _setTextValues(_loadedProfile);
      }
  }

  void _saveProfile() async {
    var newLevel = int.parse(_levelController.value.text);
    _loadedProfile = await updateProfile(
      _nameController.value.text,
      _descriptionController.value.text,
      newLevel,
      _selectedAvatarId,
      _currentToken
    );
    userState.user.profile = _loadedProfile;
    Navigator.of(context).pushReplacementNamed("welcome");
  }

  void _selectProfileAvatarId(int id) {
    setState(() {
      _selectedAvatarId = id;
    });
  }
  //todo: change multiple text inputs into a form and use this validator
  String numberValidator(String value) {
    if(value == null) {
      return null;
    }
    final n = num.tryParse(value);
    if(n == null) {
      return '"$value" is not a valid number';
    }
    return null;
  }

  Widget _makeBottom() {
    return Container(
      height: 80.0,
      child: BottomAppBar(
        color: Theme.of(context).primaryColor,
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                RaisedButton (
                  color: Theme.of(context).accentColor,
                  child: Text('Save Changes'),
                  onPressed: _saveProfile,
                )
              ]
          )
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(_titleText),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text('Select a weapon to represent your character:')
              ),
              Flexible(
                child: GridView.extent(
                  primary: false,
                  maxCrossAxisExtent: 80,
                  padding: const EdgeInsets.all(10.0),
                  mainAxisSpacing: 5.0,
                  crossAxisSpacing: 5.0,
                  children: USER_AVATARS.map((avatar) =>
                      RaisedButton(
                        color: avatar.id == _selectedAvatarId
                            ? Theme.of(context).accentColor
                            : Colors.white,
                        child: Image(
                            image: AssetImage(avatar.uri)
                        ),
                        onPressed: (){
                          _selectProfileAvatarId(avatar.id);
                        },
                      )
                  ).toList(),
                ),
              ),
              Container(
                padding: EdgeInsets.all(14.0),
                child: Column(
                  children: [
                    TextField(
                      decoration: InputDecoration(
                          labelText: 'Character Name'
                      ),
                      controller: _nameController,
                    ),
                    TextField(
                        decoration: InputDecoration(
                            labelText: 'Character Level'
                        ),
                        controller: _levelController,
                        keyboardType: TextInputType.number
                    ),
                    TextField(
                        maxLines: 4,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                            labelText: 'Character Description'
                        ),
                        controller: _descriptionController
                    ),
                  ]
                )
              )
            ]
          )
        ),
        bottomNavigationBar: _makeBottom(),
    );
  }
}