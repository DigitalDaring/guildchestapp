import 'package:flutter/material.dart';
import 'dart:async';

import 'package:pathfinder_guild_chest/services/loginService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  var _tokenText = '';
  var _loginFailed = false;

  void _attemptLogin() async {
    try {
      final loginResult = await attemptLogin(
          _emailController.value.text, _passwordController.value.text);

      userState = loginResult;

      this.setState(() {
        if(userState == null) {
          _loginFailed = true;
        } else {
          _loginFailed = false;

          if(loginResult.user.confirmedDate == null) {
            Navigator.of(context).pushReplacementNamed("confirm");
          } else {
            Navigator.of(context).pushReplacementNamed("welcome");
          }

        }
      });


    } catch(e) {
      this.setState(() {
        _tokenText = e;
      });
    }
  }

  void _goToSignup() {
    Navigator.of(context).pushNamed("signup");
  }

  void _goToConfirmation() {
    Navigator.of(context).pushNamed("confirm");
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Text(
                        'Login to Guild Chest',
                        textScaleFactor: 2.2,
                      ),
                      Image(image: AssetImage("assets/gui/chest_t_04.png")),
                      TextField(
                        decoration: InputDecoration(
                            labelText: 'Email Address'
                        ),
                        controller: _emailController,
                      ),
                      TextField(
                        decoration: InputDecoration(
                            labelText: 'Password'
                        ),
                        controller: _passwordController,
                        obscureText: true,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              FlatButton(
                                  child: Text('signup',
                                  style: TextStyle(color: Colors.black54)),
                                  onPressed: _goToSignup
                              ),
                              RaisedButton(
                                color: Theme.of(context).accentColor,
                                child: Text('Login', textScaleFactor: 1.4),
                                onPressed: _attemptLogin,
                              ),
                            ]
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              child: Text(
                                'I have a confirmation code!',
                                style: TextStyle(color: Colors.black54)
                              ),
                              onPressed: _goToConfirmation,
                            )
                          ]
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child:  Text(_loginFailed ?
                          "Sorry, that doesn't appear to be a valid login" : ""),
                      )
                    ]
                  )
            ),
            ],
          ),
        )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
