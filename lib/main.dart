import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/pages/addItemToChest.dart';
import 'package:pathfinder_guild_chest/pages/chestDetails.dart';
import 'package:pathfinder_guild_chest/pages/confirmation.dart';
import 'package:pathfinder_guild_chest/pages/craftAKey.dart';
import 'package:pathfinder_guild_chest/pages/manageGold.dart';
import 'package:pathfinder_guild_chest/pages/login.dart';
import 'package:pathfinder_guild_chest/pages/logout.dart';
import 'package:pathfinder_guild_chest/pages/profile.dart';
import 'package:pathfinder_guild_chest/pages/signup.dart';
import 'package:pathfinder_guild_chest/pages/welcome.dart';
import 'package:pathfinder_guild_chest/services/localStorageService.dart';
import 'package:pathfinder_guild_chest/state/userState.dart';
import 'package:pathfinder_guild_chest/pages/addChest.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/scheduler.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Guild Chest',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.black,
        accentColor: Colors.amberAccent
      ),
      home: MyHomePage(title: 'Guild Chest'),
        routes: <String, WidgetBuilder> {
          "signup" : (BuildContext context) => new SignupPage(),
          "confirm" : (BuildContext context) => new ConfirmPage(),
          "login" : (BuildContext context) => new LoginPage(),
          "logout": (BuildContext context) => new LogoutPage(),
          "welcome" : (BuildContext context) => new WelcomePage(),
          "profile" : (BuildContext context) => new ProfilePage(),
          "addchest" : (BuildContext context) => new AddChestPage(),
          "chestdetails" : (BuildContext context) => new ChestDetailsPage(),
          "addItemToChest" : (BuildContext context) => new AddItemToChestPage(),
          "craftAKey": (BuildContext context) => new CraftAKeyPage(),
          "manageGold": (BuildContext context) => new ManageGoldPage()
        }
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void determineNextDestination() async{
    final storedLogin = await getStoredLogin();
    if(storedLogin != null && storedLogin.user.confirmedDate != null) {
      print("already logged in!  goind to welcome page");
      userState = storedLogin;
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("welcome");
      });
    } else if (storedLogin != null && storedLogin.user.confirmedDate == null) {
      userState = storedLogin;
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("confirm");
      });
    } else {
      print("not logged in, going to login page");
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.of(context).pushReplacementNamed("login");
      });
    }
  }

  @override
  initState() {
    super.initState();
    determineNextDestination();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [],
        ),
      )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
