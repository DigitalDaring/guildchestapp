import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/classes/chestModel.dart';

class ChestComponent extends StatelessWidget {
  ChestComponent({this.chest, this.onPressed});

  final ChestModel chest;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPressed,
        child: Card(
            child: Row(
                children: [
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Image(
                          image: AssetImage("assets/Chest_02_02.png"),
                          width: 100,
                          height: 100,
                          fit: BoxFit.contain
                      )
                  ),
                  Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                          children: [
                            Text(chest.name)
                          ]
                      )
                  )
                ]
            )
        )

    );
  }

}