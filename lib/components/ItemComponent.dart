import 'package:flutter/material.dart';
import 'package:pathfinder_guild_chest/classes/storedItem.dart';

class ItemComponent extends StatelessWidget {

  ItemComponent({this.item, this.onPressed});

  final StoredItem item;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {

    var rowDetails = Row(
      children: [
        Text(item.name)
      ]
    );

    if(item.count > 1.0) {
      rowDetails.children.add(
        Text(' x' + item.count.round().toString())
      );
    }

    return InkWell (
      onTap: onPressed,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: rowDetails
      )
    );
  }

}