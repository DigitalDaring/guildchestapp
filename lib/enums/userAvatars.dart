class UserAvatar {
  final int id;
  final String uri;

  const UserAvatar({this.id, this.uri});
}

const USER_AVATARS = [
  UserAvatar(id: 1, uri: "assets/gui/profile_images/axe_01.png"),
  UserAvatar(id: 2, uri: "assets/gui/profile_images/axe_02.png"),
  UserAvatar(id: 3, uri: "assets/gui/profile_images/book_01.png"),
  UserAvatar(id: 4, uri: "assets/gui/profile_images/bow_01.png"),
  UserAvatar(id: 5, uri: "assets/gui/profile_images/bow_02.png"),
  UserAvatar(id: 6, uri: "assets/gui/profile_images/empty_01.png"),
  UserAvatar(id: 7, uri: "assets/gui/profile_images/hammer_01.png"),
  UserAvatar(id: 8, uri: "assets/gui/profile_images/knife_01.png"),
  UserAvatar(id: 9, uri: "assets/gui/profile_images/mace_01.png"),
  UserAvatar(id: 10, uri: "assets/gui/profile_images/potion_01.png"),
  UserAvatar(id: 11, uri: "assets/gui/profile_images/shield_01.png"),
  UserAvatar(id: 12, uri: "assets/gui/profile_images/staff_01.png"),
  UserAvatar(id: 13, uri: "assets/gui/profile_images/staff_02.png"),
  UserAvatar(id: 14, uri: "assets/gui/profile_images/sword_01.png"),
  UserAvatar(id: 15, uri: "assets/gui/profile_images/sword_02.png"),
];

const DEFAULT_USER_AVATAR =   UserAvatar(id: 6, uri: "assets/gui/profile_images/empty_01.png");